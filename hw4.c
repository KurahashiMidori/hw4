/* hw4.c */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#define MAX_LENGTH 5

char* rotate(char* moji, int suji){
  char* ret;
  int lng;
  int i, n;
  lng = strlen(moji);
  ret = (char*)malloc(sizeof(char) * (lng+1));
  ret[lng] = '\0';
  for(i=0; i<lng; i++){
    n = (i + suji) % lng;
    printf("i:%d, n:%d\n", i, n);  //  確認用表示
    if(n < 0){
      ret[n+lng] = moji[i];
    }else{
      ret[n] = moji[i];
    }
  }
  return ret;
}

int main(int argc, char *argv[]){
  int num;
  char *ptr;
  char *ret;

  if(argc != 3){  // 引数が足りなかったら終了
    fprintf(stderr, "Usage: ./hw4 <string> <integer>\n");
    exit(EXIT_FAILURE);
  }
  if(strlen(argv[1]) > MAX_LENGTH){  // 文字数が長すぎたら終了
    fprintf(stderr, "Too long word.\n");
    exit(EXIT_FAILURE);
  }
  // 入力された数字をcharからintに変換
  num = (int)strtol(argv[2], &ptr, 10);
  // 以下エラー処理
  if(errno == EINVAL || errno == ERANGE){
    perror("ERROR(hw4)");
    exit(EXIT_FAILURE);
  }else if(errno != 0){
    perror("Unexpected ERROR(hw4)");
    exit(EXIT_FAILURE);
  }else if(ptr == argv[2]){
    fprintf(stderr, "No number is found.");
    exit(EXIT_FAILURE);
  }
  // ここまでstrtolのエラー処理

  printf("Before : %s\n", argv[1]);
  ret = rotate(argv[1], num);
  printf("After  : %s\n", ret);
}
