/* hw4_test.c */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX_LENGTH 5

char* rotate(char* moji, int suji){
  char* ret;
  int lng;
  int i, n;
  lng = strlen(moji);
  if(lng > MAX_LENGTH){
    return "too long word";
  }
  ret = (char*)malloc(sizeof(char) * (lng+1));
  ret[lng] = '\0';
  for(i=0; i<lng; i++){
    n = (i + suji) % lng;
    //  printf("i:%d, n:%d\n", i, n);  //  確認用表示
    if(n < 0){
      ret[n+lng] = moji[i];
    }else{
      ret[n] = moji[i];
    }
  }
  return ret;
}

int main(void){
  printf("test1 :rotate(\"abcde\", 1) -> %s\n", rotate("abcde", 1));
  printf("test2 :rotate(\"abc\", 2) -> %s\n", rotate("abc", 2));
  printf("test3 :rotate(\"abcdefg\", -1) -> %s\n", rotate("abcdefg", -1));
  printf("test4 :rotate(\"abcde\", 6) -> %s\n", rotate("abcde", 6));
  printf("test5 :rotate(\"abcde\", -7) -> %s\n", rotate("abcde", -7));
}
